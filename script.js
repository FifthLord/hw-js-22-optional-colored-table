
createRect();
document.body.addEventListener('click', toggleAllColor);


function createRect() {
   document.body.insertAdjacentHTML('beforeend',
      `<div id="main-div-rect"
      style="white-space: nowrap;"
      ></div>`)

   let mainDiv = document.getElementById('main-div-rect');
   mainDiv.addEventListener('click', toggleColor);

   for (let i = 1; i < 31; i++) {
      mainDiv.insertAdjacentHTML('beforeend', `<div id='row ${i}' class='div-row' style="text-align: center;"></div>`)
   }

   Array.from(mainDiv.children).forEach(elem => {
      for (let j = 1; j < 31; j++) {
         elem.insertAdjacentHTML('beforeend', `<div class='div-item white'
         style="display: inline-block; height: 25px; width: 25px; margin-left: 3px;"
         ></div>`);
      }
   });
}


function toggleColor(event) {
   if (event.target.closest('.div-item')) {
      event.target.classList.toggle('black');
      event.cancelBubble = true;
   }
}

function toggleAllColor() {
   let divItems = document.getElementsByClassName('div-item');
   Array.from(divItems).forEach(elem => elem.classList.toggle('black'));
}


//*----таблиця-таблтця
// function createRect() {
//    document.body.insertAdjacentHTML('afterend',
//       `<table style="margin-top: 40px; margin-left: auto; margin-right: auto;">
//          <tbody id="tbody-rect">
//          </tbody>
//       </table>`)

//    let tb = document.getElementById('tbody-rect');
//    for (let i = 0; i < 30; i++) {
//       let tr = document.createElement('TR');
//       tr.innerHTML = `<tr></tr>`;
//       tb.appendChild(tr);
//    }

//    tb.addEventListener('click', toggleColor);

//    Array.from(document.getElementsByTagName("tr")).forEach(elem => {
//       for (let j = 0; j < 30; j++) {
//          let td = document.createElement('TD');
//          td.innerHTML = `<td style="margin-left: 3px;"><div class='div-item white'
//          style="height: 25px; width: 25px;"
//          ></div></td>`;
//          elem.appendChild(td);
//       }
//    });
// }